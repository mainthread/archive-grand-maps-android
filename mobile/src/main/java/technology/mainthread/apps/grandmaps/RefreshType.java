package technology.mainthread.apps.grandmaps;

public enum RefreshType {
    TYPE_FEATURED, TYPE_RANDOM
}
