# Grand Maps for Muzei
[![Circle CI](https://circleci.com/gh/AndrewJack/GrandMaps.svg?style=svg&circle-token=c83800ca8fa3364c8dfe7b264e82959aafd9fd15)](https://circleci.com/gh/AndrewJack/GrandMaps)

Grand Maps for Muzei is a cartography themed extension for the Muzei live-wallpaper.

[Trello Board](https://trello.com/b/HiXcdmk9)

## Features ##
* A featured cartography themed wallpaper every day!
* Or a random map with a custom refresh interval
* Skip to the next random map manually

## How to Use ##
1. Install Muzei @ http://get.muzei.co
2. Install Grand Maps for Muzei
3. Open the Muzei app
4. Select "Grand Maps" as your source
5. Done!